﻿using System;
using Core;

namespace DataStructure
{
    class Program
    {
        static void Main(string[] args)
        {
            var linkedList = new LinkedList<int>();
            for (var i = 0; i < 100; i++)
            {
                linkedList.AddItem(i);
            }
            Console.Write(linkedList.PrintAll());
            Console.ReadKey();
        }
    }
}
