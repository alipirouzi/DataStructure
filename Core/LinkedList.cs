﻿using System.Text;
namespace Core
{
    public class LinkedList
    {
        private LinkedListNode _head;
        public void AddItem(object data)
        {
            if (_head == null)
            {
                _head = new LinkedListNode
                {
                    Data = data
                };
            }
            else
            {
                var lastNode = TraverseToTheEnd();
                lastNode.NextNode = new LinkedListNode
                {
                    Data = data
                };
            }
        }
        private LinkedListNode TraverseToTheEnd()
        {
            if (_head == null)
            {
                return _head;
            }
            var currentNode = _head;
            while (currentNode.NextNode != null)
            {
                currentNode = currentNode.NextNode;
            }
            return currentNode;//last node in the list
        }
        public string PrintAll()
        {
            if (_head == null)
            {
                return "No element in the array.";
            }
            var currentNode = _head;
            var sb = new StringBuilder();
            while (currentNode.NextNode != null)
            {
                sb.AppendLine(currentNode.Data.ToString());
                currentNode = currentNode.NextNode;
            }
            sb.AppendLine(currentNode.Data.ToString());
            return sb.ToString();
        }
    }

    public class LinkedList<T>
    {
        private readonly LinkedList _ls;

        public LinkedList()
        {
            _ls= new LinkedList();
        }

        public void AddItem(T data)
        {
            _ls.AddItem(data);
        }

        public string PrintAll()
        {
            return _ls.PrintAll();
        }
    }

}