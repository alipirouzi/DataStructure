﻿namespace Core
{
    internal class LinkedListNode
    {
        public object Data { get; set; }
        public LinkedListNode NextNode { get; set; }
    }
}